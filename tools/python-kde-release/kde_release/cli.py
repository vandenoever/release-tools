# Copyright 2015, 2019 Jonathan Riddell <jr@jriddell.org>
# Copyright 2017, 2019 Adrian Chaves <adrian@chaves.io>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from click import command, echo, option, prompt, ClickException, DateTime

from kde_release.appstream import add_versions as _add_appstream_versions
from kde_release.bugzilla import add_versions as _add_bugzilla_versions


def handle_all_exceptions(function):
    def wrapper(*args, **kwargs):
        try:
            function(*args, **kwargs)
        except ClickException:
            raise
        except Exception as error:
            raise ClickException(repr(error))
    return wrapper


@command()
@option('-s', '--srcdir', prompt='Source folder',
        help='Folder containing local clones of the Git repositories from '
        '../modules.git')
@option('--date', prompt='Release date', type=DateTime(),
        help='Planned release date to record in AppStream files')
@option('-v', '--verbose', is_flag=True,
        help='Provide additional details about errors')
@option('-d', '--dry', is_flag=True,
        help='Do not change AppStream files in repositories')
@option('-c', '--clone', is_flag=True,
        help='Clone missing Git folders.')
@option('--hide-skipped', is_flag=True,
        help='Do not print lines for skipped products')
@handle_all_exceptions
def add_appstream_versions(srcdir, date, verbose, dry, clone, hide_skipped):
    """Adds new project versions to AppStream metainfo files.

    Speficically, for each project listed in modules.git:

    1.  CMake is used to determine the project version, as defined by CMake’s
        project command. If the version is not found, the project is skipped.

    2.  A version is added to the AppStream metainfo file of the target
        project.
    """
    _add_appstream_versions(srcdir, date, verbose, dry, clone, hide_skipped)


@command()
@option('-s', '--srcdir', prompt='Source folder',
        help='Folder containing local clones of the Git repositories from '
        'modules.git')
@option('-e', '--email', help='Email of your Bugzilla account')
@option('-p', '--password',
        help='Password of your Bugzilla account')
@option('-v', '--verbose', is_flag=True, hidden=True)
@option('-d', '--dry', is_flag=True,
        help='Do not submit anything to Bugzilla. Note: Local Git clones may '
        'be modified.')
@option('-c', '--clone', is_flag=True,
        help='Clone missing Git folders.')
@option('--hide-skipped', is_flag=True,
        help='Do not print lines for skipped products')
@handle_all_exceptions
def add_bugzilla_versions(srcdir, email, password, verbose, dry, clone,
                          hide_skipped):
    """Adds new project versions to the KDE Bugtracking System.

    Speficically, for each project listed in modules.git:

    1.  CMake is used to determine the project version, as defined by CMake’s
        project command. If the version is not found, the project is skipped.

    2.  A version is added to the target project in the KDE Bugtracking System.
    """
    if not dry:
        if email is None:
            email = prompt('Email')
        if password is None:
            password = prompt('Password', hide_input=True)
    if verbose:
        echo('WARNING: The -v/--verbose option is deprecated')
    _add_bugzilla_versions(srcdir, email, password, dry, clone, hide_skipped)
